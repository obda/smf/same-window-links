<?php

const OBDA_SWL_COPYRIGHT = "Same Window Links by obda Technologies, © 2024";


/**
 * Replace `target="_blank"` attributes from an HTML string.
 *
 * @param string $message The message string, which will be modified inline.
 * @param bool $smileys Whether to parse smileys as well.
 * @param string $cache_id The cache ID.
 * @param array $parse_tags If set, only these given BBCode tags are parsed.
 * @return void
 */
function obdaSameWindowLinksReplaceTargetBlank(
    string &$message,
    bool &$smileys,
    string &$cache_id,
    array &$parse_tags
): void {
    $pattern = "/
        (<(?:a|area|base|form)\\b[^>]*)  # inside a supported HTML element
        \\s+target=                      # the `target` attribute
        (['\"]?)_blank\\2                # single, double, or no quotes
        (?=[\\s>])                       # followed by whitespace or `>`
    /ix";
    $message = preg_replace($pattern, '$1', $message);
}


/**
 * Add credits for this modification to the global context.
 *
 * @return void
 */
function obdaSameWindowLinksCredits(): void
{
    global $context;
    $context["copyrights"]["mods"][] = OBDA_SWL_COPYRIGHT;
}
