[size=x-large][b]Same Window Links[/b][/size]

A modification for [url=https://www.simplemachines.org]Simple Machines Forum (SMF) 2.1[/url] that causes all links to open in the same window (or tab).

This modification does not change any stored data.  It merely adapts the generated output via hooks.


[size=large][b]Why?[/b][/size]

Apart from being [url=https://www.w3.org/WAI/WCAG21/Techniques/general/G200]an accessibility problem[/url], users should be able to decide themselves whether they want a link to be opened in a new window or tab.


[size=large][b]Settings[/b][/size]

There are no settings for this mod.  Uninstall it to disable its functionality.


[size=large][b]License[/b][/size]

Same Window Links is released under the BSD-3-Clause license.


[size=large][b]Changelog[/b][/size]

Version 1.1.2 (2024-05-06):
[list]
[li]Add a prefix to the internal copyright constant’s name to avoid naming collisions.[/li]
[/list]

Version 1.1.1 (2024-05-04):
[list]
[li]Fix copyright notice.[/li]
[/list]

Version 1.1.0 (2024-05-03):
[list]
[li]Add modification credits to copyright notice.[/li]
[li]Fix package installation issues.[/li]
[/list]

Version 1.0.0 (2024-05-02):
[list]
[li]First public release.[/li]
[/list]


[size=large][b]Repository[/b][/size]

https://gitlab.com/obda/smf/same-window-links
