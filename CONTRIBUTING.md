Contributing
============

Development Dependencies
------------------------

You will need the following tools for development:

  * [just][1]
  * Docker
  * [pre-commit][2]

[1]: https://just.systems
[2]: https://pre-commit.com


### just

If [just][1] is not installed on your system, run the following to install the
single binary to the *~/.local/bin* directory:

    curl --proto "=https" --tlsv1.2 -sSf https://just.systems/install.sh \
        | bash -s -- --tag 1.25.2 --to ~/.local/bin

Afterwards, invoke `just help` from anywhere inside the repository directory
tree to list the available commands.


### pre-commit

Running [pre-commit][2] locally is not strictly necessary, but it will allow
you to automatically perform a number of checks that will also run in the
GitLab CI pipeline whenever creating a new Git commit.  Once you have it
installed, you need to invoke

    pre-commit install

from the repository root directory just one single time.


Code Style
----------

Execute [PHP_CodeSniffer][3] to check for [PSR-12][4] compliance:

    just lint

By default, it will check all PHP files.

[3]: https://github.com/PHPCSStandards/PHP_CodeSniffer
[4]: https://www.php-fig.org/psr/psr-12/


Tests
-----

Running the test suite requires Docker:

    just test

Additional arguments are directly passed through to [PHPUnit][5].

[5]: https://phpunit.de
