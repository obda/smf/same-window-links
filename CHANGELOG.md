Changelog
=========

Version 1.1.2
-------------

Released 2024-05-06

  * Add a prefix to the internal copyright constant’s name to avoid naming
    collisions.


Version 1.1.1
-------------

Released 2024-05-04

  * Fix copyright notice.


Version 1.1.0
-------------

Released 2024-05-03

  * Add modification credits to copyright notice.
  * Fix package installation issues.


Version 1.0.0
-------------

Released 2024-05-02

  * First public release.
