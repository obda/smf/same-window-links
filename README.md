Same Window Links
=================

A modification for [Simple Machines Forum (SMF) 2.1][1] that causes all links
to open in the same window (or tab).

This modification does not change any stored data.  It merely adapts the
generated output via hooks.

[1]: https://www.simplemachines.org


Why?
----

Apart from being [an accessibility problem][2], users should be able to decide
themselves whether they want a link to be opened in a new window or tab.

[2]: https://www.w3.org/WAI/WCAG21/Techniques/general/G200
