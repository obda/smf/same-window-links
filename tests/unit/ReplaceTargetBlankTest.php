<?php

declare(strict_types=1);

namespace obda\SameWindowLinks;

use PHPUnit\Framework\TestCase;

/**
 * Tests for the `obdaSameWindowLinksReplaceTargetBlank()` function.
 */
final class ReplaceTargetBlankTest extends TestCase
{
    /**
     * Return examples for attribute removal, along with the expected results.
     *
     * @return array[]
     */
    public static function correctRemovalProvider(): array
    {
        return [
            # Different types of attribute quoting
            [
                "<a href='https://example.com' target='_blank'>Example</a>",
                "<a href='https://example.com'>Example</a>",
            ],
            [
                '<a href="https://example.com" target="_blank">Example</a>',
                '<a href="https://example.com">Example</a>',
            ],
            [
                '<a href="https://example.com" target=_blank>Example</a>',
                '<a href="https://example.com">Example</a>',
            ],
            # Additional whitespace after the attribute
            [
                '<a href="https://example.com" target="_blank" >Example</a>',
                '<a href="https://example.com" >Example</a>',
            ],
            # Different attribute order
            [
                '<a target="_blank" href="https://example.com">Example</a>',
                '<a href="https://example.com">Example</a>',
            ],
            # Different casing
            [
                '<a href="https://example.com" TARGET="_blank">Example</a>',
                '<a href="https://example.com">Example</a>',
            ],
            [
                '<a href="https://example.com" target="_BLANK">Example</a>',
                '<a href="https://example.com">Example</a>',
            ],
            [
                '<a href="https://example.com" TARGET="_BLANK">Example</a>',
                '<a href="https://example.com">Example</a>',
            ],
            [
                '<a href="https://example.com" Target="_Blank">Example</a>',
                '<a href="https://example.com">Example</a>',
            ],
            # With newlines
            [
                "<a href='https://example.com' target='_blank'\n>Example</a>",
                "<a href='https://example.com'\n>Example</a>",
            ],
            [
                "<a\nhref='https://example.com' target='_blank'\n>Example</a>",
                "<a\nhref='https://example.com'\n>Example</a>",
            ],
            # No other attributes (bogus case, just to make sure)
            [
                '<a target="_blank">Example</a>',
                '<a>Example</a>',
            ],
        ];
    }

    /**
     * Verify correct removal of `target="_blank"` HTML attributes.
     *
     * @param string $message The string to perform removal on.
     * @param string $expected The expected resulting string after removal.
     * @return void
     *
     * @dataProvider correctRemovalProvider
     */
    public function testCorrectRemoval(
        string $message,
        string $expected
    ): void {
        $smileys = true;
        $cache_id = "";
        $parse_tags = [];
        obdaSameWindowLinksReplaceTargetBlank(
            $message,
            $smileys,
            $cache_id,
            $parse_tags
        );
        $this->assertSame($expected, $message);
    }

    /**
     * Return examples where no attribute removal should take place.
     *
     * @return array[]
     */
    public static function noRemovalProvider(): array
    {
        return [
            # No `target` attribute
            ['<a href="https://example.com">Example</a>'],
            # `target` attribute with a different value
            ['<a href="https://example.com" target="_self">Example</a>'],
            # `target="_blank"` in the link text
            ['<a href="https://example.com">Example target="_blank" </a>'],
            # Unsupported element
            ['<foo href="https://example.com" target="_blank">Example</foo>'],
        ];
    }

    /**
     * Verify that no attribute removal is applied for a given string.
     *
     * @param string $message The string to test.
     * @return void
     *
     * @dataProvider noRemovalProvider
     */
    public function testNoRemoval(string $message): void
    {
        $expected = $message;  # Store a copy, as the hook modifies `$message`
        $smileys = true;
        $cache_id = "";
        $parse_tags = [];
        obdaSameWindowLinksReplaceTargetBlank(
            $message,
            $smileys,
            $cache_id,
            $parse_tags
        );
        $this->assertSame($expected, $message);
    }
}
